package com.UHF.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBOpenHelper extends SQLiteOpenHelper {  
    public DBOpenHelper(Context context) {  
        super(context, "t_wms.db", null, 1);  
    }  
  
    //数据库第一次创建时候调用，  
    public void onCreate(SQLiteDatabase db) {  
        db.execSQL("create table baseSet(id integer primary key autoincrement, url varchar(50))");  
    }  
  
    //数据库文件版本号发生变化时调用  
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {  
  
    }  
}  