package com.UHF.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.UHF.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


public class MyAdapter extends BaseAdapter{
	
	private Context mContext;
	private List<String> mList;
	private LayoutInflater layoutInflater;
	private Map<String,Integer> data=new HashMap<String, Integer>();
	
	public MyAdapter(Context context, Map<String,Integer> datas) {
		mContext = context;
		mList = new ArrayList(datas.keySet());
		data=datas;
		layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewParent) {
		// TODO Auto-generated method stub
		ItemView iv = null;
		if(view == null){
			iv = new ItemView();
			view = layoutInflater.inflate(R.layout.list, null);
			iv.tvCode = (TextView)view.findViewById(R.id.list_lable);
			iv.tvNum = (TextView)view.findViewById(R.id.list_number);
			view.setTag(iv);
		}else{
			iv = (ItemView)view.getTag();
		}
		iv.tvCode.setText(mList.get(position));
		iv.tvNum.setText(data.get(mList.get(position)).toString());
		return view;
	}
	
	public class ItemView{
		TextView tvCode;
		TextView tvNum;
	}

	public List<String> getmList() {
		return mList;
	}

	public void setmList(List<String> mList) {
		this.mList = mList;
	}

	public Map<String, Integer> getData() {
		return data;
	}

	public void setData(Map<String, Integer> data) {
		setmList(new ArrayList(data.keySet()));
		this.data = data;
	}
	
}