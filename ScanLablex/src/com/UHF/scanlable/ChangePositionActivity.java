package com.UHF.scanlable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.UHF.R;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.model.CheckBoxAdapter;
import com.UHF.model.EmployBean;
import com.UHF.model.MyAdapter;
import com.UHF.turntable.CircleActivity;
import com.UHF.turntable.ScanActivity;
import com.UHF.util.CheckDataFromServer;
import org.apache.commons.lang.StringUtils;


public class ChangePositionActivity extends ScanActivity implements OnClickListener{
	//扫物料
	private Button cp_scanMaterial_bt;
	//扫托盘
	private Button cp_scanTray_bt;
	//扫库位
	private Button cp_scanStock_bt;
	//确定
	private Button checkIt_bt;
	//重新绑定
	private Button reBinding_bt;
	//切换物料/成品  0：物料1成品
	private Button change_type_bt;
	
//	private Button QRcodeBubmit;
	//标题
	private TextView cp_title;
	/**
	 * 0:解绑，1绑定
	 */
	private String type="0";
	/**
	 * 0:物料，1成品
	 */
	private String change_type="0";
	/**
	 * 按钮类型//1物料2托盘3库位
	 */
	private String lastBt="-1";
	
	private String operateType="0";
	
	private ListView listView;
	private AcceptanceAdapter myAdapter;
	private CheckBoxAdapter checkAdapter;
	private Map<String,Integer> data=new HashMap<String, Integer>();
	private Map<String,Integer> oldData=new HashMap<String, Integer>();
	/**
	 * 流水号或者编号，类型（物料，仓库，库位，托盘等）
	 */
	private LinkedHashMap<Object,Object> listData=new LinkedHashMap<Object,Object>();
	/**
	 * 新增数据
	 */
	private LinkedHashMap<Object,Object> newListData=new LinkedHashMap<Object,Object>();
	

	private LinkedHashMap<Object,Object> materialListData=new LinkedHashMap<Object,Object>();
	/**
	 * 物料流水号.状态
	 */
	private LinkedHashMap<Object,Object> materiaCheckData=new LinkedHashMap<Object,Object>();
	private LinkedHashMap<Object,Object> changeListData=new LinkedHashMap<Object,Object>();
	private Intent intent=null;
	private String ip;
	private Handler mHandler;
	private boolean pass=false;
	private ImageView setUrlBt;
	private int QR_requestCode=2;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.change_position);
		cp_scanMaterial_bt = (Button)findViewById(R.id.cp_scanMaterial_bt);
		cp_scanMaterial_bt.setOnClickListener(this);
		cp_scanTray_bt = (Button)findViewById(R.id.cp_scanTray_bt);
		cp_scanTray_bt.setOnClickListener(this);
//		cp_scanStock_bt = (Button)findViewById(R.id.cp_scanStock_bt);
//		cp_scanStock_bt.setOnClickListener(this);
		reBinding_bt = (Button)findViewById(R.id.reBinding_bt);
		reBinding_bt.setOnClickListener(this);
		change_type_bt = (Button)findViewById(R.id.change_type_bt);
		change_type_bt.setOnClickListener(this);
		checkIt_bt = (Button)findViewById(R.id.checkIt_bt);
		checkIt_bt.setOnClickListener(this);
		//标题
		cp_title = (TextView)findViewById(R.id.cp_title);//
		listView = (ListView)findViewById(R.id.check_position_list);//
//		QRcodeBubmit = (Button)findViewById(R.id.QRcode_button);
//		QRcodeBubmit.setOnClickListener(this);
		intent = new Intent();
		mHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
    		super.handleMessage(msg);
			switch (msg.what) {
			case 0:
				if(isCanceled) return;
				if("-1".equals(lastBt)){//非扫码状态
					return;
				}
				data = UfhData.scanResult6c;
				if(data==null||data.isEmpty()){
					break;
				}
				List checkList=checkMapData(oldData,data);
				if(checkList.size()>0){
					CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
					String s=StringUtils.join(checkList.toArray(), ",");
					Map m=new HashMap();
					m.put("scanCode", s);
					m.put("user_id", UfhData.getEmployBean().getId());
					m.put("type", type);//解绑扫物料
					if(("1".equals(lastBt)||"-1".equals(lastBt))&&"0".equals(change_type)){
						checkDataFromServer.setOperType("1");
					}else if(("1".equals(lastBt)||"-1".equals(lastBt))&&"1".equals(change_type)){//扫成品
						checkDataFromServer.setOperType("30");
					}else if("2".equals(lastBt)){//扫托盘
						checkDataFromServer.setOperType("2");
					}else if("3".equals(lastBt)){//扫库位
						checkDataFromServer.setOperType("3");
					}
					checkDataFromServer.setData(m);
					checkDataFromServer.setIp(UfhData.getIP());
					checkDataFromServer.setmHandler(handler);
					checkDataFromServer.setWhat(0);
					if(checkDataFromServer.checkData()){
						oldData=new HashMap<String, Integer>(data);
					}
				}
				break;
			case 1:
				break;
			case 2:
				break;
			default:
				break;
			}
		}
	};
	}
//	private boolean checkMaterial(Map<String,Integer> map1){
//		boolean flag=false;
//		for(String key:map1.keySet()){  
//			if(materiaCheckData.get(materialListData.get(key))!=null){
//				if("已赋码".equals(materiaCheckData.get(materialListData.get(key)))||"已入托".equals(materiaCheckData.get(materialListData.get(key)))){
//					materiaCheckData.put(materialListData.get(key), "已扫描");
//					flag=true;
//				}
//			}
//		}
//		return flag;
//	}
//	private boolean checkTray(Map<String,Integer> map1){
//		boolean flag=false;
//		for(String key:map1.keySet()){  
//			if(trayCheckData.get(trayListData.get(key))!=null){
//				if("待扫描".equals(trayCheckData.get(trayListData.get(key)))){
//					trayCheckData.put(trayListData.get(key), "已扫描");
//					flag=true;
//				}
//			}
//		}
//		return flag;
//	}
	@Override
	protected void showScanCode() {
		// TODO Auto-generated method stub
		super.showScanCode();
		Toast.makeText(ChangePositionActivity.this,getBarcodeStr(),Toast.LENGTH_SHORT).show();
		data.put(getBarcodeStr(), 1);
		List checkList=checkMapData(oldData,data);
		if(checkList.size()>0){
			CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
			String s=StringUtils.join(checkList.toArray(), ",");
			Map m=new HashMap();
			m.put("scanCode", s);
			m.put("user_id", UfhData.getEmployBean().getId());
			m.put("type", type);//解绑扫物料
			//扫物料
			if(("1".equals(lastBt)||"-1".equals(lastBt))&&"0".equals(change_type)){
				checkDataFromServer.setOperType("1");
			}else if(("1".equals(lastBt)||"-1".equals(lastBt))&&"1".equals(change_type)){//扫成品
				checkDataFromServer.setOperType("30");
			}else if("2".equals(lastBt)){//扫托盘
				checkDataFromServer.setOperType("2");
			}else if("3".equals(lastBt)){//扫库位
				checkDataFromServer.setOperType("3");
			}
			checkDataFromServer.setData(m);
			checkDataFromServer.setIp(UfhData.getIP());
			checkDataFromServer.setmHandler(handler);
			checkDataFromServer.setWhat(0);
			if(checkDataFromServer.checkData()){
				oldData=new HashMap<String, Integer>(data);
			}
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if("-1".equals(lastBt)){
			Intent intent = new Intent(this, CircleActivity.class); 
	        startActivity(intent);
	        finish();
		}else{
			Toast.makeText(ChangePositionActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
		}
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.cp_scanMaterial_bt://扫物料
			if("1".equals(lastBt)){
				lastBt="-1";
				cp_scanMaterial_bt.setText(R.string.scanPack);
				closeRFIDScan();
			}else if("-1".equals(lastBt)){
				lastBt="1";//查物料
				materialListData.clear();
				data.clear();
				oldData.clear();
				listData.clear();
				newListData.clear();
				myAdapter = new AcceptanceAdapter(ChangePositionActivity.this, listData);
				myAdapter= null;
				cp_scanMaterial_bt.setText(R.string.stop);
				if("0".equals(change_type)){
					openRFIDScan(mHandler,UfhData.scanResult6c,0,"0001");
				}else{
					openRFIDScan(mHandler,UfhData.scanResult6c,0,"0004");
				}
			}else{
				Toast.makeText(ChangePositionActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.cp_scanTray_bt://扫托盘
			if("2".equals(lastBt)){
				data.clear();
				oldData.clear();
				lastBt="-1";
				cp_scanTray_bt.setText(R.string.sacnTray);
				closeRFIDScan();
			}else if("-1".equals(lastBt)){
				operateType="1";//物料换托盘
				lastBt="2";//查托盘
				changeListData.clear();
				data.clear();
				oldData.clear();
				listData.clear();
				newListData.clear();
				checkAdapter= null;
				cp_scanTray_bt.setText(R.string.stop);
				openRFIDScan(mHandler,UfhData.scanResult6c,0,"0002");
			}else{
				Toast.makeText(ChangePositionActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.reBinding_bt:
			Intent intentx=new Intent(this,ChangeBandingListActivity.class);
			intentx.putExtra("change_type", change_type);//类型（0物料 1成品）
//    		intent.putExtra(MainActivity.EXTRA_EPC, myAdapter.getmList().get(position));
    		startActivity(intentx);
    		finish();
			break;
		case R.id.change_type_bt:
			if("1".equals(change_type)){
				change_type="0";
				cp_title.setText(R.string.changPositionTitle1);
			}else if("0".equals(change_type)){
				change_type="1";
				cp_title.setText(R.string.changPositionTitle3);
			}
			break;
		case R.id.checkIt_bt://确定
			List list=new ArrayList<String>();
			List incomingList=new ArrayList<String>();
			for(Object key:materialListData.keySet()){  
				list.add(key);
				incomingList.add(materialListData.get(key));
			} 
			CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
			Map m=new HashMap();
			String s=StringUtils.join(incomingList.toArray(), ",");
			String epc=StringUtils.join(list.toArray(), ",");
			m.put("scanCode", epc);
			m.put("incomingPackageId", s);
			m.put("operateType", operateType);
			m.put("type", type);//解绑/绑定
			m.put("user_id", UfhData.getEmployBean().getId());
			m.put("change_type", change_type);
			if(checkAdapter!= null&&changeListData.size()>0&&!"".equals(checkAdapter.getCheckData())){
				checkAdapter.getCheckData();
				m.put("trayEpcId", changeListData.get(checkAdapter.getCheckData()));
			}else if("1".equals(type)){
				Toast.makeText(ChangePositionActivity.this,"未扫码库位/托盘!",Toast.LENGTH_SHORT).show();
				break;
			}
			if("-1".equals(lastBt)){
				checkDataFromServer.setOperType("31");//换库位
				checkDataFromServer.setData(m);
				checkDataFromServer.setIp(UfhData.getIP());
				checkDataFromServer.setmHandler(handler);
				checkDataFromServer.setWhat(1);
				if(checkDataFromServer.checkData()){
					oldData=new HashMap<String, Integer>(data);
				}
			}else{
				Toast.makeText(ChangePositionActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		default:
			break;
		}	
		
		
	}
//	@Override
//	public void onClick(View v) {
//		// TODO Auto-generated method stub
//		switch (v.getId()) {
//		case R.id.cp_scanMaterial_bt://扫物料
//			if("1".equals(lastBt)){
//				lastBt="-1";
//				cp_scanMaterial_bt.setText(R.string.scan);
//				closeRFIDScan();
//			}else if("-1".equals(lastBt)){
//				lastBt="1";//查物料
//				materialListData.clear();
//				data.clear();
//				oldData.clear();
//				listData.clear();
//				newListData.clear();
//				myAdapter = new AcceptanceAdapter(ChangePositionActivity.this, listData);
//				myAdapter= null;
//				cp_scanMaterial_bt.setText(R.string.stop);
//				if("0".equals(change_type)){
//					openRFIDScan(mHandler,UfhData.scanResult6c,0,"0001");
//				}else{
//					openRFIDScan(mHandler,UfhData.scanResult6c,0,"0004");
//				}
//			}else{
//				Toast.makeText(ChangePositionActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
//			}
//			break;
//		case R.id.cp_scanTray_bt://扫托盘
//			if("2".equals(lastBt)){
//				lastBt="-1";
//				cp_scanTray_bt.setText(R.string.sacnTray);
//				closeRFIDScan();
//			}else if("-1".equals(lastBt)){
//				operateType="1";//物料换托盘
//				lastBt="2";//查托盘
//				changeListData.clear();
//				data.clear();
//				oldData.clear();
//				listData.clear();
//				newListData.clear();
//				checkAdapter= null;
//				cp_scanTray_bt.setText(R.string.stop);
//				openRFIDScan(mHandler,UfhData.scanResult6c,0,"0002");
//			}else{
//				Toast.makeText(ChangePositionActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
//			}
//			break;
////		case R.id.cp_scanStock_bt://扫库位
////			if("3".equals(lastBt)){
////				cp_scanStock_bt.setText(R.string.scanStock);
////				lastBt="-1";
////				closeRFIDScan();
////			}else if("-1".equals(lastBt)){
////				operateType="3";//物料换库位
////				lastBt="3";//查库位
////				changeListData.clear();
////				data.clear();
////				oldData.clear();
////				listData.clear();
////				newListData.clear();
////				checkAdapter= null;
////				cp_scanStock_bt.setText(R.string.stop);
////				openRFIDScan(mHandler,UfhData.scanResult6c,0,"0003");
////			}else{
////				Toast.makeText(ChangePositionActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
////			}
////			break;
//		case R.id.checkIt_bt://确定
//			List list=new ArrayList<String>();
//			List incomingList=new ArrayList<String>();
//			for(Object key:materialListData.keySet()){  
//				list.add(key);
//				incomingList.add(materialListData.get(key));
//			} 
//			CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
//			Map m=new HashMap();
//			String s=StringUtils.join(incomingList.toArray(), ",");
//			String epc=StringUtils.join(list.toArray(), ",");
//			m.put("scanCode", epc);
//			m.put("incomingPackageId", s);
//			m.put("operateType", operateType);
//			m.put("type", type);//解绑/绑定
//			m.put("user_id", UfhData.getEmployBean().getId());
//			m.put("change_type", change_type);
//			if(checkAdapter!= null&&changeListData.size()>0&&!"".equals(checkAdapter.getCheckData())){
//				checkAdapter.getCheckData();
//				m.put("changeRpcId", changeListData.get(checkAdapter.getCheckData()));
//			}else if("1".equals(type)){
//				Toast.makeText(ChangePositionActivity.this,"未扫码库位/托盘!",Toast.LENGTH_SHORT).show();
//				break;
//			}
//			if("-1".equals(lastBt)){
//				checkDataFromServer.setOperType("31");//换库位
//				checkDataFromServer.setData(m);
//				checkDataFromServer.setIp(UfhData.getIP());
//				checkDataFromServer.setmHandler(handler);
//				checkDataFromServer.setWhat(1);
//				if(checkDataFromServer.checkData()){
//					oldData=new HashMap<String, Integer>(data);
//				}
//			}else{
//				Toast.makeText(ChangePositionActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
//			}
//			break;
//		case R.id.relieve_bt://解绑
//			if("-1".equals(lastBt)){
//				materialListData.clear();
//				data.clear();
//				oldData.clear();
//				listData.clear();
//				newListData.clear();
//				myAdapter = new AcceptanceAdapter(ChangePositionActivity.this, listData);
//				checkAdapter=  new CheckBoxAdapter(ChangePositionActivity.this, listData);
//				myAdapter.notifyDataSetChanged();
//				checkAdapter.notifyDataSetChanged();
//				myAdapter = null;
//				checkAdapter= null;
//				type="0";
//				if("0".equals(type)&&"0".equals(change_type)){
//					cp_title.setText(R.string.changPositionTitle1);
//				}else if("1".equals(type)&&"0".equals(change_type)){
//					cp_title.setText(R.string.changPositionTitle2);
//				}else if("0".equals(type)&&"1".equals(change_type)){
//					cp_title.setText(R.string.changPositionTitle3);
//				}else if("1".equals(type)&&"1".equals(change_type)){
//					cp_title.setText(R.string.changPositionTitle4);
//				}
//			}
//			break;
//		case R.id.reBinding_bt://重新绑定
//			Intent intentx=new Intent(this,ChangeBandingListActivity.class);
//			intentx.putExtra("change_type", change_type);//类型（0物料 1成品）
////    		intent.putExtra(MainActivity.EXTRA_EPC, myAdapter.getmList().get(position));
//    		startActivity(intentx);
//    		finish();
//			break;
//		case R.id.change_type_bt://切换 物料/成品
//			if("-1".equals(lastBt)){
//				materialListData.clear();
//				data.clear();
//				oldData.clear();
//				listData.clear();
//				newListData.clear();
//				myAdapter = new AcceptanceAdapter(ChangePositionActivity.this, listData);
//				checkAdapter=  new CheckBoxAdapter(ChangePositionActivity.this, listData);
//				myAdapter.notifyDataSetChanged();
//				checkAdapter.notifyDataSetChanged();
//				myAdapter = null;
//				checkAdapter= null;
//				if("0".equals(type)&&"0".equals(change_type)){
//					change_type="1";
//					cp_title.setText(R.string.changPositionTitle3);
//					change_type_bt.setText(R.string.material_product1);
//					cp_scanMaterial_bt.setText(R.string.scanProduct);
//				}else if("1".equals(type)&&"0".equals(change_type)){
//					change_type="1";
//					cp_title.setText(R.string.changPositionTitle4);
//					change_type_bt.setText(R.string.material_product1);
//					cp_scanMaterial_bt.setText(R.string.scanProduct);
//				}else if("0".equals(type)&&"1".equals(change_type)){
//					change_type="0";
//					cp_title.setText(R.string.changPositionTitle1);
//					change_type_bt.setText(R.string.material_product2);
//					cp_scanMaterial_bt.setText(R.string.scan);
//				}else if("1".equals(type)&&"1".equals(change_type)){
//					change_type="0";
//					cp_title.setText(R.string.changPositionTitle2);
//					change_type_bt.setText(R.string.material_product2);
//					cp_scanMaterial_bt.setText(R.string.scan);
//				}
//			}
//			break;
//		default:
//			break;
//		}
//		return;
//	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return super.onKeyDown(keyCode, event);
	}
	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		super.handleMessage(msg);
                switch (msg.what) {
    			case 0:
    				try {
    					String s= (String) msg.obj;
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(ChangePositionActivity.this,"扫码错误!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    		    				String operType=ss.getString("operType");
    							objList = new JSONArray(ss.getString("list"));
    							String typeName="";
    							String types="";
    							if("1".equals(operType)){
    								typeName="packageInventoryNum";
    								types="物料";
    			                }else if("2".equals(operType)){
    								typeName="code";
    								types="托盘";
    			                }else if("3".equals(operType)){
    								typeName="positionCode";
    								types="库位";
    			                }else if("30".equals(operType)){
    								typeName="packageInventoryNum";
    								types="成品";
    			                }
    							for (int i = 0; i< objList.length(); i++) {
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    				                JSONObject obj = objList.getJSONObject(i);
    				                /*acceptanceData.put(checkHex(Long.toHexString(Long.parseLong(obj.get("scanCode").toString())).toUpperCase(),"0001"),obj.get("packageInventoryNum"));//epc,流水号
    				                String backGroundColor=obj.get("backGroundColor").equals("0")?"已复核":"未复核";
    				                acceptanceCheckData.put(obj.get("packageInventoryNum").toString(),backGroundColor);//epc,流水号*/
    				                if(!"1".equals(operType)&&!"30".equals(operType)){
    				                	changeListData.put(obj.getString(typeName), obj.getString("scanCode"));
        				                newListData.put(obj.get(typeName).toString(), types);
    									//materialListData.put(obj.getString("scanCode"), obj.get(typeName).toString());
//    				                }else if(!(!obj.isNull("occupancyModule")&&!"".equals(obj.get("occupancyModule").toString())&&"0".equals(type))){
    				                }else {
        				                newListData.put(obj.get(typeName).toString(), types);
    									materialListData.put(obj.getString("scanCode"), obj.get(typeName).toString());
    				                }
    				             }
    				            listData.putAll(newListData);
								if("1".equals(operType)||"30".equals(operType)){
									if(myAdapter == null){
										myAdapter = new AcceptanceAdapter(ChangePositionActivity.this, listData);
										listView.setAdapter(myAdapter);
									}else{
										myAdapter.setData(listData) ;
									}
									myAdapter.notifyDataSetChanged();
								  }else{
									if(checkAdapter == null){
										checkAdapter = new CheckBoxAdapter(ChangePositionActivity.this, listData);
										listView.setAdapter(checkAdapter);
									}else{
										checkAdapter.setData(listData) ;
									}
									checkAdapter.notifyDataSetChanged();
								}
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    						
    			            
    					}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(ChangePositionActivity.this,"扫码错误!",Toast.LENGTH_SHORT).show();
    				}
    				break;
    			case 1:
    				try {
    					String s= (String) msg.obj;
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(ChangePositionActivity.this,"扫码错误!",Toast.LENGTH_SHORT).show();
    					}else{
    						String re="操作成功";
							JSONObject ss= new JSONObject(s);
							String operType=ss.getString("operType");
							String change_typex=ss.getString("change_type");
							String type=ss.getString("type");
							String result=ss.getString("result");
							if("0".equals(result)){
								if("0".equals(type)&&"0".equals(change_typex)){
									Toast.makeText(ChangePositionActivity.this,R.string.changPositionTitle1+"完成",Toast.LENGTH_SHORT).show();
								}else if("1".equals(type)&&"0".equals(change_typex)){
									Toast.makeText(ChangePositionActivity.this,R.string.changPositionTitle2+"完成",Toast.LENGTH_SHORT).show();
								}else if("0".equals(type)&&"1".equals(change_typex)){
									Toast.makeText(ChangePositionActivity.this,R.string.changPositionTitle3+"完成",Toast.LENGTH_SHORT).show();
								}else if("1".equals(type)&&"1".equals(change_typex)){
									Toast.makeText(ChangePositionActivity.this,R.string.changPositionTitle4+"完成",Toast.LENGTH_SHORT).show();
								}
								Intent intent = new Intent(ChangePositionActivity.this, ChangePositionActivity.class);  
						        startActivity(intent); 
								finish();
							}else if("15".equals(result)){
								Toast.makeText(ChangePositionActivity.this,"包装占用！",Toast.LENGTH_SHORT).show();
							}else if("-1".equals(result)){
								Toast.makeText(ChangePositionActivity.this,"操作错误！",Toast.LENGTH_SHORT).show();
							}else if("-2".equals(result)){
								Toast.makeText(ChangePositionActivity.this,"托盘/库位有误！",Toast.LENGTH_SHORT).show();
							}else if("4".equals(result)){
								Toast.makeText(ChangePositionActivity.this,"该库位已冻结！",Toast.LENGTH_SHORT).show();
							}else if("7".equals(result)){
								Toast.makeText(ChangePositionActivity.this,"托盘未入库！",Toast.LENGTH_SHORT).show();
							}else {
								Toast.makeText(ChangePositionActivity.this,"操作错误!",Toast.LENGTH_SHORT).show();
							}
						}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(ChangePositionActivity.this,"操作错误!",Toast.LENGTH_SHORT).show();
					}
    				break;
    			case 2:
    				break;
    			default:
    				break;
    			}
            }

    };
}
