package com.UHF.scanlable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.UHF.R;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.util.CheckDataFromServer;

public class InventoryBacthActivity extends Activity implements OnClickListener{

	Button refresh;
	Button incomingMButton;
	/**
	 * 单据号/状态
	 */
	private LinkedHashMap<Object,Object> listData=new LinkedHashMap<Object,Object>();
	private LinkedHashMap<Object,Object> listDatailData=new LinkedHashMap<Object,Object>();
	private AcceptanceAdapter myAdapter;
	private ListView listView;
	private Intent intent;
	//标题
	private TextView ib_title;
	private String serialNo;
	private String inventoryRegularId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		serialNo = getIntent().getStringExtra("serialNo");
		inventoryRegularId = getIntent().getStringExtra("inventoryRegularId");
		setContentView(R.layout.inventory_batch_list);
		refresh = (Button)findViewById(R.id.inventoryBatch_refresh);
		refresh.setOnClickListener(this);
		listView = (ListView)findViewById(R.id.inventoryBatchList);//
		ib_title = (TextView)findViewById(R.id.ib_title);//
		ib_title.setText(ib_title.getText()+":"+serialNo);
		intent = new Intent(this,InventoryPackageActivity.class);
		listView.setOnItemClickListener(new OnItemClickListener(){  
			  
            @Override  
            public void onItemClick(AdapterView<?> parent, View view,  
                    int position, long id) {  
        		String inventoryNumber = myAdapter.getmList().get(position);
        		LinkedHashMap<Object,Object> m=(LinkedHashMap<Object, Object>) listDatailData.get(inventoryNumber);
        		intent.putExtra("typex", (String)m.get("typex"));//类型（1物料 2托盘3库位4成品）
        		intent.putExtra("inventoryNumber", inventoryNumber);
        		intent.putExtra("inventoryRegularId", inventoryRegularId);
        		intent.putExtra("serialNo", serialNo);
//        		intent.putExtra(MainActivity.EXTRA_EPC, myAdapter.getmList().get(position));
        		startActivity(intent);
        		finish();
            }  
        });  
		refreshList();
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==refresh.getId()){
			myAdapter = null;
			listData.clear();
			refreshList();
		}
	}
	public boolean refreshList(){
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
		m.put("user_id", UfhData.getEmployBean().getId());
		m.put("inventoryRegularId", inventoryRegularId);
		checkDataFromServer.setOperType("33");//成品入库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(0);
		checkDataFromServer.checkData();
		return true;
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, InventoryListActivity.class); 
		intent.putExtra("inventoryRegularId", inventoryRegularId);
		intent.putExtra("serialNo", serialNo);
        startActivity(intent);
        finish();
	}

	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		super.handleMessage(msg);
                switch (msg.what) {
    			case 0:
    				try {
    					String s= (String) msg.obj;
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(InventoryBacthActivity.this,"单据号有误!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    		    				String operType=ss.getString("operType");
    							objList = new JSONArray(ss.getString("list"));
    							String status="";
    							for (int i = 0; i< objList.length(); i++) {
    								LinkedHashMap<Object,Object> map=new LinkedHashMap<Object,Object>();
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    				                JSONObject obj = objList.getJSONObject(i);
    				                /*acceptanceData.put(checkHex(Long.toHexString(Long.parseLong(obj.get("epcId").toString())).toUpperCase(),"0001"),obj.get("packageInventoryNum"));//epc,流水号
    				                String backGroundColor=obj.get("backGroundColor").equals("0")?"已复核":"未复核";
    				                acceptanceCheckData.put(obj.get("packageInventoryNum").toString(),backGroundColor);//epc,流水号*/
    				                status=obj.getString("status");
    				                if("0".equals(status)){
    				                	status="未盘点";
    				                }else if("1".equals(status)){
    				                	status="进行中";
    				                }else if("2".equals(status)){
    				                	status="已盘点";
    				                }
    				                try {
    				                	map.put("typex", obj.get("typex"));
    				                	map.put("status", obj.get("status"));
    				                	map.put("inventoryNumber", obj.get("inventoryNumber"));
    				                	if(!obj.isNull("taskPerson")){
        				                	map.put("taskPerson", obj.get("taskPerson"));
    				                	}
    					                listData.put(obj.get("inventoryNumber"), status);
    					                listDatailData.put(obj.get("inventoryNumber"), map);
    								} catch (Exception e) {
    									// TODO: handle exception
    								}
    				             }
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    						if(myAdapter == null){
    							myAdapter = new AcceptanceAdapter(InventoryBacthActivity.this, listData);
    							listView.setAdapter(myAdapter);
    						}else{
    							myAdapter.setData(listData) ;
    						}
    						myAdapter.notifyDataSetChanged();
    					}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(InventoryBacthActivity.this,"单据号有误!",Toast.LENGTH_SHORT).show();
    				}
    				break;
    			case 1:
    				break;
    			case 2:
    				break;
    			default:
    				break;
    			}
            }

    };
}
