package com.UHF.scanlable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.UHF.R;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.turntable.ScanActivity;
import com.UHF.util.CheckDataFromServer;

public class InventoryPackageActivity extends ScanActivity implements OnClickListener{

	Button refresh;
	Button scanItx;
	Button inventoryPackage_checked;
	/**
	 * 单据号/状态
	 */
	private LinkedHashMap<Object,Object> listDetailData=new LinkedHashMap<Object,Object>();
	private Map<String,Integer> data=new HashMap<String, Integer>();
	private Map<String,Integer> oldData=new HashMap<String, Integer>();
	/**
	 * 新扫描确认的数据
	 */
	private List<String> materialAddData=new ArrayList<String>();
	/**
	 * epcid/物料流水号
	 */
	private LinkedHashMap<Object,Object> materialListData=new LinkedHashMap<Object,Object>();
	
	/**
	 * 物料流水号.状态
	 */
	private LinkedHashMap<Object,Object> materiaCheckData=new LinkedHashMap<Object,Object>();
	private AcceptanceAdapter myAdapter;
	private ListView listView;
	private Intent intent;
	private String inventoryNumber;
	private String inventoryRegularId;
	//标题
	private TextView ip_title;
	private Handler mHandler;
	private String typex;
	private String lastBt="0";
	private String serialNo;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.inventory_package_list);
		inventoryNumber = getIntent().getStringExtra("inventoryNumber");
		inventoryRegularId = getIntent().getStringExtra("inventoryRegularId");
		serialNo = getIntent().getStringExtra("serialNo");
		typex= getIntent().getStringExtra("typex");
		refresh = (Button)findViewById(R.id.inventoryPackage_refresh);
		refresh.setOnClickListener(this);
		inventoryPackage_checked = (Button)findViewById(R.id.inventoryPackage_checked);
		inventoryPackage_checked.setOnClickListener(this);
		scanItx = (Button)findViewById(R.id.scanItx);
		scanItx.setOnClickListener(this);
		
		listView = (ListView)findViewById(R.id.inventoryPackageList);//
		ip_title = (TextView)findViewById(R.id.ip_title);//
		ip_title.setText(ip_title.getText()+":"+inventoryNumber);
		intent = new Intent(this,PackageInfoActivity.class);
		listView.setOnItemClickListener(new OnItemClickListener(){  
			  
            @Override  
            public void onItemClick(AdapterView<?> parent, View view,  
                    int position, long id) {  
        		String iNumber = myAdapter.getmList().get(position);
        		LinkedHashMap<Object,Object> m=(LinkedHashMap<Object, Object>) listDetailData.get(iNumber);
        		/*Intent intentx = null;
        		//类型（1物料 2托盘3库位4成品）
        		if("1".equals(typex)){
        			intentx=intent1;
        		}else if("2".equals(typex)){
        			intentx=intent2;
        		}else if("3".equals(typex)){
        			intentx=intent3;
        		}else if("4".equals(typex)){
        			intentx=intent4;
        		}*/
        		intent.putExtra("scanCode", (String)m.get("scanCode"));
        		intent.putExtra("inventoryRegularId", inventoryRegularId);
        		intent.putExtra("inventoryNumber", inventoryNumber);
        		intent.putExtra("typex", typex);
        		intent.putExtra("serialNo", serialNo);
//        		intent.putExtra(MainActivity.EXTRA_EPC, myAdapter.getmList().get(position));
        		startActivity(intent);
        		finish();
            }  
        });  
		mHandler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
	    		super.handleMessage(msg);
				switch (msg.what) {
				case 0:
					if(isCanceled) return;
					break;
				case 1:
					if(isCanceled) return;
					data = UfhData.scanResult6c;
					if(data==null||data.isEmpty()){
						break;
					}
					if(checkMaterial(data)){
						if(myAdapter == null){
							myAdapter = new AcceptanceAdapter(InventoryPackageActivity.this, materiaCheckData);
							listView.setAdapter(myAdapter);
						}else{
							myAdapter.setData(materiaCheckData) ;
						}
						myAdapter.notifyDataSetChanged();
					}
					break;
				default:
					break;
				}
				
			}
			
		};
		refreshList();
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==refresh.getId()){
			if("0".equals(lastBt)){
				myAdapter = null;
				materialListData.clear();
				refreshList();
			}else{
				Toast.makeText(InventoryPackageActivity.this,"请先停止扫码!",Toast.LENGTH_SHORT).show();
			}
		}else if(v.getId()==inventoryPackage_checked.getId()){
			if("0".equals(lastBt)){
				submitPackage();
			}else{
				Toast.makeText(InventoryPackageActivity.this,"请先停止扫码!",Toast.LENGTH_SHORT).show();
			}
		}else if(v.getId()==scanItx.getId()){
			if("0".equals(lastBt)){
				lastBt="1";
				scanItx.setText(R.string.stop);
				openRFIDScan(mHandler,UfhData.scanResult6c,1,"000"+typex);
			}else{
				lastBt="0";
				scanItx.setText(R.string.scan);
				closeRFIDScan();
			}
		}
		
		
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, InventoryBacthActivity.class); 
		intent.putExtra("serialNo", serialNo);
		intent.putExtra("inventoryRegularId", inventoryRegularId);
        startActivity(intent);
        finish();
	}
	public boolean submitPackage(){
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
		m.put("user_id", UfhData.getEmployBean().getId());
		m.put("typex", typex);//1物料 4成品
		m.put("types", "0");//0改状态，1改数量+状态
		m.put("inventoryRegularId", inventoryRegularId);
		m.put("inventoryNumber", inventoryNumber);
		m.put("scanCode", StringUtils.join(materialAddData.toArray(), ","));
		checkDataFromServer.setOperType("36");//成品入库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(1);
		checkDataFromServer.checkData();
		return true;
	}
	
	public boolean refreshList(){
		materiaCheckData.clear();
		materialListData.clear();
		data.clear();
		materialAddData.clear();
		listDetailData.clear();
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
		m.put("user_id", UfhData.getEmployBean().getId());
		m.put("inventoryNumber", inventoryNumber);
		m.put("inventoryRegularId", inventoryRegularId);
		checkDataFromServer.setOperType("35");//成品入库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(0);
		checkDataFromServer.checkData();
		return true;
	}
	private boolean checkMaterial(Map<String,Integer> map1){
		boolean type=false;
		for(String key:map1.keySet()){  
			if(materiaCheckData.get(materialListData.get(key))!=null){
				if("未盘点".equals(materiaCheckData.get(materialListData.get(key)))){
					materiaCheckData.put(materialListData.get(key), "已盘点");
					materialAddData.add(key);
					type=true;
				}
			}
		}
		return type;
	}
	@Override
	protected void showScanCode() {
		// TODO Auto-generated method stub
		super.showScanCode();
		Toast.makeText(InventoryPackageActivity.this,getBarcodeStr(),Toast.LENGTH_SHORT).show();
		data.put(getBarcodeStr(), 1);
		if(checkMaterial(data)){
			if(myAdapter == null){
				myAdapter = new AcceptanceAdapter(InventoryPackageActivity.this, materiaCheckData);
				listView.setAdapter(myAdapter);
			}else{
				myAdapter.setData(materiaCheckData) ;
			}
			myAdapter.notifyDataSetChanged();
		}
	}
	
	


	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		super.handleMessage(msg);
                switch (msg.what) {
    			case 0:
    				try {
    					String s= (String) msg.obj;
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(InventoryPackageActivity.this,"批次号有误!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    		    				String operType=ss.getString("operType");
    							objList = new JSONArray(ss.getString("list"));
    							String status="";
    							for (int i = 0; i< objList.length(); i++) {
    								LinkedHashMap<Object,Object> map=new LinkedHashMap<Object,Object>();
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    				                JSONObject obj = objList.getJSONObject(i);
    				                /*acceptanceData.put(checkHex(Long.toHexString(Long.parseLong(obj.get("epcId").toString())).toUpperCase(),"0001"),obj.get("packageInventoryNum"));//epc,流水号
    				                String backGroundColor=obj.get("backGroundColor").equals("0")?"已复核":"未复核";
    				                acceptanceCheckData.put(obj.get("packageInventoryNum").toString(),backGroundColor);//epc,流水号*/
    				                if(obj.isNull("taskPerson")||"".equals(obj.get("taskPerson"))){
    				                	status="未盘点";
    				                }else {
    				                	status="已盘点";
    				                }
    				                try {
    				                	map.put("scanCode", obj.get("scanCode"));
    				                	map.put("packageInventoryNum", obj.get("packageInventoryNum"));
    				                	if(!obj.isNull("taskPerson")){
        				                	map.put("taskPerson", obj.get("taskPerson"));
    				                	}
    					                materialListData.put(obj.get("packageInventoryNum"), status);
    					                listDetailData.put(obj.get("packageInventoryNum"), map);
    					                materialListData.put(obj.getString("scanCode"),obj.getString("packageInventoryNum"));
    				                	materiaCheckData.put(obj.getString("packageInventoryNum"), status);
    								} catch (Exception e) {
    									// TODO: handle exception
    								}
    				             }
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    						if(myAdapter == null){
    							myAdapter = new AcceptanceAdapter(InventoryPackageActivity.this, materiaCheckData);
    							listView.setAdapter(myAdapter);
    						}else{
    							myAdapter.setData(materiaCheckData) ;
    						}
    						myAdapter.notifyDataSetChanged();
    					}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(InventoryPackageActivity.this,"批次号有误!",Toast.LENGTH_SHORT).show();
    				}
    				break;
    			case 1:
    				try {
						String s=(String) msg.obj;
						if(s==null||"1".equals(s)){
							Toast.makeText(InventoryPackageActivity.this,"盘点失败!",Toast.LENGTH_SHORT).show();
						}else{
							try {
								String re="";
								JSONObject ss= new JSONObject(s);
								String operType=ss.getString("operType");
								String result=ss.getString("result");
								if("0".equals(result)){
									Toast.makeText(InventoryPackageActivity.this,"盘点成功",Toast.LENGTH_SHORT).show();
									refreshList();
								}else {
									Toast.makeText(InventoryPackageActivity.this,"盘点失败",Toast.LENGTH_SHORT).show();
								}
							} catch (Exception e) {
								// TODO: handle exception
								Toast.makeText(InventoryPackageActivity.this,"保存失败",Toast.LENGTH_SHORT).show();
							}
						}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(InventoryPackageActivity.this,"盘点失败!",Toast.LENGTH_SHORT).show();
					}
    				break;
    			case 2:
    				break;
    			default:
    				break;
    			}
            }

    };
}
