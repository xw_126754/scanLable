package com.UHF.scanlable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.UHF.R;
import com.UHF.model.EmployBean;
import com.UHF.scanlable.UfhData.UhfGetData;
import com.UHF.turntable.CircleActivity;
import com.UHF.util.CheckDataFromServer;
import com.UHF.util.DBDao;

import android.R.integer;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.ActivityGroup;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class LoginActivity extends Activity implements OnClickListener {
	private LinearLayout submit;
	private EditText et_account;
	private EditText et_psw;
	private TextView loginButton;
	private Intent intent=null;
	private String ip;
	private Handler mHandler;
	private boolean pass=false;
	private ImageView setUrlBt;
	private String localIp;//设备名称
	private int tty_speed = 57600;
	private byte addr = (byte) 0xff; 
	private String[] strBand =new String[5]; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
 		setContentView(R.layout.login);
		submit = (LinearLayout)findViewById(R.id.lay_login);
		submit.setOnClickListener(this);
		et_account = (EditText)findViewById(R.id.et_account);
		et_psw = (EditText)findViewById(R.id.et_psw);
		loginButton= (TextView)findViewById(R.id.login);
		setUrlBt= (ImageView)findViewById(R.id.iv_top);
		setUrlBt.setOnClickListener(this);   
		//获取wifi服务  
        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);  
        //判断wifi是否开启  
        if (!wifiManager.isWifiEnabled()) {  
        wifiManager.setWifiEnabled(true);    
        }  
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();       
        int ipAddress = wifiInfo.getIpAddress();   
        localIp = intToIp(ipAddress);  
		intent = new Intent();
		mHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
    		super.handleMessage(msg);
			try {
				JSONObject obj=new JSONObject((String)msg.obj);
				String reslut=obj.getString("result");
				String info=obj.getString("info");
				if("0".equals(reslut)){
					EmployBean employBean=new EmployBean();
					employBean.setCompanyType(obj.getString("companyType"));
					employBean.setId(obj.getString("id"));
					employBean.setWorkShopId(obj.getString("workShopId"));
					employBean.setUserNo(obj.getString("userNo"));
					employBean.setUserName(obj.getString("userName"));
					UfhData.setEmployBean(employBean);
			        intent.setClass(LoginActivity.this, CircleActivity.class);
					startActivity(intent);
					finish();
				}else{
					Toast.makeText(LoginActivity.this,info,Toast.LENGTH_SHORT).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Toast.makeText(LoginActivity.this,"登录失败!",Toast.LENGTH_SHORT).show();
			}catch (Exception e) {
				// TODO: handle exception
				Toast.makeText(LoginActivity.this,"服务器连接超时!",Toast.LENGTH_SHORT).show();
			}
	        pass=false;
			loginButton.setText("登录");
//			if(isCanceled) return;
//			switch (msg.what) {
//			case MSG_UPDATE_LISTVIEW:
//				if(mode.equals(MainActivity.TABLE_6B)){
//					data = checkMap(UfhData.scanResult6c);
//				}else {
//					if("0".equals(sacnType)){
//						data = UfhData.scanResult6c;
//					}else if("3".equals(sacnType)){//包装详情
//						data = checkMap(UfhData.scanResult6c);
//					}
////					data = UfhData.scanResult6c;
//				}
//				if(data==null||data.isEmpty()){
//					break;
//				}
//				if(myAdapter == null){
//					myAdapter = new MyAdapter(ScanMode.this, new ArrayList(data.keySet()));
//					listView.setAdapter(myAdapter);
//				}else{
//					myAdapter.mList = new ArrayList(data.keySet());
//				}
//				txNum.setText(String.valueOf(myAdapter.getCount()));
//				myAdapter.notifyDataSetChanged();
//				break;
//
//			default:
//				break;
//			}
//			super.handleMessage(msg);
		}

	};
	/*new Thread(new Runnable() {

		@Override
		public void run() {
			int result = UhfGetData
					.OpenUhf(tty_speed, addr, 4, 0, null);
			if (result == 0) {
				UhfGetData.GetUhfInfo();
//				mHandler.removeMessages(MSG_SHOW_PROPERTIES);
//				mHandler.sendEmptyMessage(MSG_SHOW_PROPERTIES);
			}
		}
	}).start();*/
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iv_top:
			intent.setClass(LoginActivity.this, setIpActivity.class);
			startActivity(intent);
			break;
		case R.id.lay_login:
			ip=UfhData.getIP();
			DBDao d=new DBDao(this); 
			if(ip==null){ 
				ip=d.find(); 
				if(ip==null){ 
					ip="http://10.4.4.199:8080/gmp-fy";
					d.save(ip);
				}
			}else{
				d.update(ip);
			}
			UfhData.setIP(ip);
			final String psw=et_psw.getText().toString();
			final String account=et_account.getText().toString();
			if(!pass){
				pass=true;
				loginButton.setText("登录中...");
				CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
				Map m=new HashMap();
				m.put("user_no", account);//账号
				m.put("password", psw);//密码
				m.put("companyType", "1");//公司
				m.put("pcName", localIp);//pcName
				checkDataFromServer.setOperType("10");//物料入托
				checkDataFromServer.setData(m);
				checkDataFromServer.setIp(UfhData.getIP());
				checkDataFromServer.setmHandler(mHandler);
				checkDataFromServer.setWhat(1);
				checkDataFromServer.checkData();
/*				new Thread(new Runnable(){
					public void run(){
						List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>(); 
						HttpUtil h=new HttpUtil();
			    		h.setUrl(UfhData.getIP());
						nameValuePairs.add(new BasicNameValuePair("operType", "10"));//物料入托
						Map serialNo=UfhData.getSerialNo();
				        nameValuePairs.add(new BasicNameValuePair("user_no", account));//账号
				        nameValuePairs.add(new BasicNameValuePair("password",psw ));//密码
				        nameValuePairs.add(new BasicNameValuePair("companyType","1" ));//公司
				        nameValuePairs.add(new BasicNameValuePair("pcName",localIp ));//pcName
						JSONObject json;
				        try {
							json = new JSONObject(h.getHttpResult(nameValuePairs));
							Message m=new Message();
							m.obj=json;
							m.what=1;
							mHandler.sendMessage(m);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						}
					}).start();*/	
		}
			break;
		default:
			break;
		}
		return;
	}
	private String intToIp(int i) {       
        
        return (i & 0xFF ) + "." +       
      ((i >> 8 ) & 0xFF) + "." +       
      ((i >> 16 ) & 0xFF) + "." +       
      ( i >> 24 & 0xFF) ;  
   } 
}
