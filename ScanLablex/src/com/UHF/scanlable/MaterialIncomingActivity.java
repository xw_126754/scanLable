package com.UHF.scanlable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.UHF.R;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.model.CheckBoxAdapter;
import com.UHF.model.EmployBean;
import com.UHF.model.MyAdapter;
import com.UHF.turntable.CircleActivity;
import com.UHF.turntable.ScanActivity;
import com.UHF.util.CheckDataFromServer;
import org.apache.commons.lang.StringUtils;


public class MaterialIncomingActivity extends ScanActivity implements OnClickListener{
	//扫物料
	private Button mi_scanMaterial_bt;
	//扫托盘
	private Button mi_scanTray_bt;
	//扫库位
	private Button mi_scanStock_bt;
	//入托
	private Button materialIntoStory;
	//物料入库
	private Button materialIntoTray;
	//托盘入库
	private Button trayIntoStory;
	
	//确定
	private Button mi_checkIt_bt;
//	private Button QRcodeBubmit;
	//标题
	private TextView mi_title;
	//个数
	private TextView allCounts;
	//个数
	private Integer counts= 0;
	/**
	 * 0:入托，1托盘入库2物料入库
	 */
	private String type="0";
	/**
	 * 按钮类型//1物料2托盘3库位
	 */
	private String lastBt="-1";
	
	private String operateType="0";
	
	private ListView listView;
	private AcceptanceAdapter myAdapter;
	private CheckBoxAdapter checkAdapter;
	private Map<String,Integer> data=new HashMap<String, Integer>();
	private Map<String,Integer> oldData=new HashMap<String, Integer>();
	/**
	 * epcid/物料流水号
	 */
	private LinkedHashMap<Object,Object> materialListData=new LinkedHashMap<Object,Object>();
	
	/**
	 * 物料流水号.状态
	 */
	private LinkedHashMap<Object,Object> materiaCheckData=new LinkedHashMap<Object,Object>();
	/**
	 * epcid/托盘编号
	 */
	private LinkedHashMap<Object,Object> trayListData=new LinkedHashMap<Object,Object>();
	
	/**
	 * 托盘编号.状态
	 */
	private LinkedHashMap<Object,Object> trayCheckData=new LinkedHashMap<Object,Object>();
	/**
	 * epcid/仓库编号
	 */
	private LinkedHashMap<Object,Object> storageListData=new LinkedHashMap<Object,Object>();
	
	/**
	 * 仓库流水号.状态
	 */
	private LinkedHashMap<Object,Object> storageCheckData=new LinkedHashMap<Object,Object>();
	private Handler mHandler;
	private String documentNo;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.material_incoming);
		documentNo = getIntent().getStringExtra("documentNo");
		mi_scanMaterial_bt = (Button)findViewById(R.id.mi_scanMaterial_bt);
		mi_scanMaterial_bt.setOnClickListener(this);
		mi_scanTray_bt = (Button)findViewById(R.id.mi_scanTray_bt);
		mi_scanTray_bt.setOnClickListener(this);
		mi_scanStock_bt = (Button)findViewById(R.id.mi_scanStock_bt);
		mi_scanStock_bt.setOnClickListener(this);
		mi_checkIt_bt = (Button)findViewById(R.id.mi_checkIt_bt);
		mi_checkIt_bt.setOnClickListener(this);
		materialIntoStory = (Button)findViewById(R.id.materialIntoStory);
		materialIntoStory.setOnClickListener(this);
		materialIntoTray = (Button)findViewById(R.id.materialIntoTray);
		materialIntoTray.setOnClickListener(this);
		trayIntoStory = (Button)findViewById(R.id.trayIntoStory);
		trayIntoStory.setOnClickListener(this);

		//标题
		mi_title = (TextView)findViewById(R.id.mi_title);//
		listView = (ListView)findViewById(R.id.mi_list);//
		allCounts = (TextView)findViewById(R.id.allCounts);//
		
			mHandler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
	    		super.handleMessage(msg);
				switch (msg.what) {
				case 0:
					if(isCanceled) return;
					/*if(!("2".equals(lastBt)||"3".equals(lastBt))){//非扫托盘或库位
						return;
					}
					data = UfhData.scanResult6c;
					if(data==null||data.isEmpty()){
						break;
					}
					List checkList=checkMapData(oldData,data);
					if(checkList.size()>0){
						CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
						String s=StringUtils.join(checkList.toArray(), ",");
						Map m=new HashMap();
						m.put("scanCode", s);
						m.put("user_id", UfhData.getEmployBean().getId());
	//					m.put("type", "0");//解绑扫物料
						checkDataFromServer.setOperType(lastBt);//换库位
						checkDataFromServer.setData(m);
						checkDataFromServer.setIp(UfhData.getIP());
						checkDataFromServer.setmHandler(handler);
						checkDataFromServer.setWhat(0);
						if(checkDataFromServer.checkData()){
							oldData=new HashMap<String, Integer>(data);
						}
					}*/
					break;
				case 1:
					if(isCanceled) return;
					data = UfhData.scanResult6c;
					if(data==null||data.isEmpty()){
						break;
					}
					checkMaterial(data);
					if(myAdapter == null){
						myAdapter = new AcceptanceAdapter(MaterialIncomingActivity.this, materiaCheckData);
						listView.setAdapter(myAdapter);
					}else{
						myAdapter.setData(materiaCheckData) ;
					}
					allCounts.setText("已扫描个数："+counts);
					myAdapter.notifyDataSetChanged();
	//				Toast.makeText(AcceptanceActivity.this,new ArrayList(data.keySet()).size(),Toast.LENGTH_SHORT).show();
					break;
				case 2:
					if(isCanceled) return;
					data = UfhData.scanResult6c;
					if(data==null||data.isEmpty()){
						break;
					}
					if("0".equals(type)){
						List checkList=checkMapData(oldData,data);
						if(checkList.size()>0){
							CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
							String s=StringUtils.join(checkList.toArray(), ",");
							Map m=new HashMap();
							m.put("scanCode", s);
							m.put("user_id", UfhData.getEmployBean().getId());
		//					m.put("type", "0");//解绑扫物料
							checkDataFromServer.setOperType("2");
							checkDataFromServer.setData(m);
							checkDataFromServer.setIp(UfhData.getIP());
							checkDataFromServer.setmHandler(handler);
							checkDataFromServer.setWhat(2);
							if(checkDataFromServer.checkData()){
								oldData.putAll(data);
							}
						}
					}else if("1".equals(type)){
						checkTray(data);
						if(myAdapter == null){
							myAdapter = new AcceptanceAdapter(MaterialIncomingActivity.this, trayCheckData);
							listView.setAdapter(myAdapter);
						}else{
							myAdapter.setData(trayCheckData) ;
						}
						allCounts.setText("已扫描个数："+counts);
						myAdapter.notifyDataSetChanged();
					}
					break;
				case 3:
					if(isCanceled) return;
					data = UfhData.scanResult6c;
					if(data==null||data.isEmpty()){
						break;
					}
//					if("0".equals(type)){
						List checkList=checkMapData(oldData,data);
						if(checkList.size()>0){
							CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
							String s=StringUtils.join(checkList.toArray(), ",");
							Map m=new HashMap();
							m.put("scanCode", s);
							m.put("user_id", UfhData.getEmployBean().getId());
	//						m.put("type", "0");//解绑扫物料
							checkDataFromServer.setOperType("3");
							checkDataFromServer.setData(m);
							checkDataFromServer.setIp(UfhData.getIP());
							checkDataFromServer.setmHandler(handler);
							checkDataFromServer.setWhat(3);
							if(checkDataFromServer.checkData()){
								oldData.putAll(data);
							}
						}
//					}else if("1".equals(type)){
//						checkTray(data);
//						if(myAdapter == null){
//							myAdapter = new AcceptanceAdapter(ProductIncomingActivity.this, storageCheckData);
//							listView.setAdapter(myAdapter);
//						}else{
//							myAdapter.setData(storageCheckData) ;
//						}
//						myAdapter.notifyDataSetChanged();
//					}
					break;
				case 4:
					try {
						String s=(String) msg.obj;
						if(s==null||"1".equals(s)){
							Toast.makeText(MaterialIncomingActivity.this,"物料入库失败!",Toast.LENGTH_SHORT).show();
						}else{
							try {
								String re="操作成功";
								JSONObject ss= new JSONObject(s);
								String operType=ss.getString("operType");
								String result=ss.getString("result");
								if("0".equals(result)){
									if("8".equals(operType)){
										re=documentNo+"单据物料入托成功！";
									}else if("9".equals(operType)){
										re=documentNo+"单据托盘入库成功！";
									}else if("6".equals(operType)){
										re=documentNo+"单据物料入库成功！";
									}
									Toast.makeText(MaterialIncomingActivity.this,re,Toast.LENGTH_SHORT).show();
									onBackPressed();
								}else if("-1".equals(result)){
									if("8".equals(operType)){
										re="扫码的物料包含已经入库/入托的！";
									}else if("9".equals(operType)){
										re="扫码的托盘包含已经入库的！";
									}else if("6".equals(operType)){
										re="扫码的物料包含已经入库的！";
									}
									Toast.makeText(MaterialIncomingActivity.this,re,Toast.LENGTH_SHORT).show();
								}else if("-2".equals(result)){
									if("21".equals(operType)){
										re="托盘被占用！";
									}else if("22".equals(operType)){
										re="库位有误！";
									}else if("23".equals(operType)){
										re="库位有误！";
									}
									Toast.makeText(MaterialIncomingActivity.this,re,Toast.LENGTH_SHORT).show();
								}else{
									Toast.makeText(MaterialIncomingActivity.this,documentNo+"入库失败！",Toast.LENGTH_SHORT).show();
								}
							} catch (Exception e) {
								// TODO: handle exception
								Toast.makeText(MaterialIncomingActivity.this,"物料入库失败!",Toast.LENGTH_SHORT).show();
							}
						}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(MaterialIncomingActivity.this,"物料入库失败!",Toast.LENGTH_SHORT).show();
					}
	//				Toast.makeText(AcceptanceActivity.this,new ArrayList(data.keySet()).size(),Toast.LENGTH_SHORT).show();
					break;
				default:
					break;
				}
				
			}
			
		};
		refreshList();
	}
	
	public boolean refreshList(){
		materialListData.clear();
		materiaCheckData.clear();
		trayListData.clear();
		trayCheckData.clear();
		storageListData.clear();
		storageCheckData.clear();
		myAdapter= null;
		checkAdapter= null;
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
		m.put("user_id", UfhData.getEmployBean().getId());
		m.put("documentNo", documentNo);
		checkDataFromServer.setOperType("4");//物料入库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(0);
		checkDataFromServer.checkData();
		return true;
	}
	public boolean refreshTrayList(){
		materialListData.clear();
		materiaCheckData.clear();
		trayListData.clear();
		trayCheckData.clear();
		storageListData.clear();
		storageCheckData.clear();
		myAdapter= null;
		checkAdapter= null;
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
		m.put("user_id", UfhData.getEmployBean().getId());
		m.put("documentNo", documentNo);
		checkDataFromServer.setOperType("12");//物料入库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(2);
		checkDataFromServer.checkData();
		return true;
	}
	
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.mi_scanMaterial_bt://扫物料
			if("1".equals(lastBt)){
				lastBt="-1";
				mi_scanMaterial_bt.setText(R.string.scan);
				closeRFIDScan();
				data.clear();
				oldData.clear();
			}else if("-1".equals(lastBt)){
				lastBt="1";//查物料
				mi_scanMaterial_bt.setText(R.string.stop);
				openRFIDScan(mHandler,UfhData.scanResult6c,1,"0001");
			}else{
				Toast.makeText(MaterialIncomingActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.mi_scanTray_bt://扫托盘
			if("2".equals(lastBt)){
				lastBt="-1";
				mi_scanTray_bt.setText(R.string.sacnTray);
				closeRFIDScan();
				data.clear();
				oldData.clear();
			}else if("-1".equals(lastBt)){
				if("0".equals(type)){
					operateType="1";//物料换托盘
					trayListData.clear();
					trayCheckData.clear();
					checkAdapter= null;
					if(checkAdapter == null){
						checkAdapter = new CheckBoxAdapter(MaterialIncomingActivity.this, trayCheckData);
						listView.setAdapter(checkAdapter);
					}else{
						checkAdapter.setData(trayCheckData) ;
					}
					allCounts.setText("已扫描个数："+counts);
					checkAdapter.notifyDataSetChanged();
					mi_scanTray_bt.setText(R.string.stop);
					openRFIDScan(mHandler,UfhData.scanResult6c,2,"0002");
				}else if("1".equals(type)){
					mi_scanTray_bt.setText(R.string.stop);
					openRFIDScan(mHandler,UfhData.scanResult6c,2,"0002");
				}
				lastBt="2";//查托盘
			}else{
				Toast.makeText(MaterialIncomingActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.mi_scanStock_bt://扫库位
			if("3".equals(lastBt)){
				mi_scanStock_bt.setText(R.string.scanStock);
				lastBt="-1";
				closeRFIDScan();
				data.clear();
				oldData.clear();
			}else if("-1".equals(lastBt)){
				operateType="3";//物料换库位
				lastBt="3";//查库位
				storageListData.clear();
				storageCheckData.clear();
				if(checkAdapter == null){
					checkAdapter = new CheckBoxAdapter(MaterialIncomingActivity.this, storageListData);
					listView.setAdapter(checkAdapter);
				}else{
					checkAdapter.setData(storageListData) ;
				}
				allCounts.setText("已扫描个数："+counts);
				checkAdapter.notifyDataSetChanged();
				mi_scanStock_bt.setText(R.string.stop);
				openRFIDScan(mHandler,UfhData.scanResult6c,3,"0003");
			}else{
				Toast.makeText(MaterialIncomingActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.mi_checkIt_bt://确定
			CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
			Map m=new HashMap();
			m.put("documentNo", documentNo);
			m.put("operateType", operateType);
			m.put("user_id", UfhData.getEmployBean().getId());
			if("-1".equals(lastBt)){
				if("0".equals(type)){
					try {
						m.put("materialScanCode",  checkResult(materialListData,materiaCheckData));
						checkDataFromServer.setOperType("8");//物料入托
						m.put("trayScanCode", getKey(trayListData,checkAdapter.getCheckData()));
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(MaterialIncomingActivity.this,"未选中托盘!",Toast.LENGTH_SHORT).show();
						return;
					}
				}else if("1".equals(type)){
					try {
						m.put("trayScanCode", checkResult(trayListData,trayCheckData));
						checkDataFromServer.setOperType("9");//托盘入库
						m.put("storageScanCode", getKey(storageListData,checkAdapter.getCheckData()));
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(MaterialIncomingActivity.this,"未选中库位!",Toast.LENGTH_SHORT).show();
						return;
					}
				}else if("2".equals(type)){
					try {
						m.put("materialScanCode",  checkResult(materialListData,materiaCheckData));
						checkDataFromServer.setOperType("6");//物料入库
						m.put("storageScanCode", getKey(storageListData,checkAdapter.getCheckData()));
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(MaterialIncomingActivity.this,"未选中库位!",Toast.LENGTH_SHORT).show();
						return;
					}
				}
				checkDataFromServer.setData(m);
				checkDataFromServer.setIp(UfhData.getIP());
				checkDataFromServer.setmHandler(mHandler);
				checkDataFromServer.setWhat(4);
				checkDataFromServer.checkData();
			}else{
				Toast.makeText(MaterialIncomingActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.materialIntoTray://入托
			if("-1".equals(lastBt)){
				refreshList();
				mi_title.setText(R.string.materialList1);
				type="0";
			}else{
				Toast.makeText(MaterialIncomingActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.trayIntoStory://托入库
			if("-1".equals(lastBt)){
				refreshTrayList();
				type="1";
				mi_title.setText(R.string.materialList2);
			}else{
				Toast.makeText(MaterialIncomingActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.materialIntoStory://物料入库
			if("-1".equals(lastBt)){
				refreshList();
				type="2";
				mi_title.setText(R.string.materialList3);
			}else{
				Toast.makeText(MaterialIncomingActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		default:
			break;
		}
		return;
	}
	private boolean checkMaterial(Map<String,Integer> map1){
		boolean flag=false;
		counts=0;
		for(String key:map1.keySet()){  
			if(materiaCheckData.get(materialListData.get(key))!=null){
				if("已赋码".equals(materiaCheckData.get(materialListData.get(key)))||"已入托".equals(materiaCheckData.get(materialListData.get(key)))){
					materiaCheckData.put(materialListData.get(key), "已扫描");
					flag=true;
				}
			}
		}
		for(Object key:materiaCheckData.keySet()){  
			if("已扫描".equals(materiaCheckData.get(key))){
				counts++;
			}
		}
		return flag;
	}
	private boolean checkTray(Map<String,Integer> map1){
		boolean flag=false;
		for(String key:map1.keySet()){  
			if(trayCheckData.get(trayListData.get(key))!=null){
				if("待扫描".equals(trayCheckData.get(trayListData.get(key)))){
					trayCheckData.put(trayListData.get(key), "已扫描");
					flag=true;
				}
			}
		}
		counts=0;
		for(Object key:trayCheckData.keySet()){  
			if("已扫描".equals(trayCheckData.get(key))){
				counts++;
			}
		}
		return flag;
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
//            if (requestCode == QR_requestCode) {
                 String QRcode = data.getStringExtra("QRcode");
                 //设置结果显示框的显示数值
                 Toast.makeText(MaterialIncomingActivity.this,QRcode,Toast.LENGTH_SHORT).show();
//             }
	}
	@Override
	protected void showScanCode() {
		// TODO Auto-generated method stub
		super.showScanCode();
		Toast.makeText(MaterialIncomingActivity.this,getBarcodeStr(),Toast.LENGTH_SHORT).show();
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		data.put(getBarcodeStr(), 1);
		Map m=new HashMap();
		m.put("scanCode", getBarcodeStr());
		m.put("user_id", UfhData.getEmployBean().getId());
//		m.put("type", "0");//解绑扫物料
		if("1".equals(lastBt)||"-1".equals(lastBt)){
			checkMaterial(data);
			if(myAdapter == null){
				myAdapter = new AcceptanceAdapter(MaterialIncomingActivity.this, materiaCheckData);
				listView.setAdapter(myAdapter);
			}else{
				myAdapter.setData(materiaCheckData) ;
			}
			allCounts.setText("已扫描个数："+counts);
			myAdapter.notifyDataSetChanged();
//			if(checkDataFromServer.checkData()){
//				oldData.putAll(data);
//			}
			return;
		}else if("2".equals(lastBt)){
			if("2".equals(type)){
				return;
			}
			if(!checkTray(data)){
//				if("0".equals(type)){
					checkDataFromServer.setOperType("2");//托盘入库
//				} else if("1".equals(type)){
//					m.put("documentNo", getBarcodeStr());
//					checkDataFromServer.setOperType("12");//托盘入库
//				}
				checkDataFromServer.setWhat(2);
			}else{
				if(myAdapter == null){
					myAdapter = new AcceptanceAdapter(MaterialIncomingActivity.this, trayCheckData);
					listView.setAdapter(myAdapter);
				}else{
					myAdapter.setData(trayCheckData) ;
				}
				allCounts.setText("已扫描个数："+counts);
				myAdapter.notifyDataSetChanged();
				return;
			}
//			if("0".equals(type)){
//				checkDataFromServer.setOperType("2");//托盘入库
//				checkDataFromServer.setWhat(2);
//			}else if("1".equals(type)){
//				checkTray(data);
//				if(myAdapter == null){
//					myAdapter = new AcceptanceAdapter(MaterialIncomingActivity.this, materiaCheckData);
//					listView.setAdapter(myAdapter);
//				}else{
//					myAdapter.setData(materiaCheckData) ;
//				}
//				myAdapter.notifyDataSetChanged();
//				return;
//			}
		}else if("3".equals(lastBt)||"-1".equals(lastBt)){
			checkDataFromServer.setOperType("3");//物料入库
			checkDataFromServer.setWhat(3);
		}
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		if(checkDataFromServer.checkData()){
			oldData.putAll(data);
		}
	}
	private String checkResult(Map map,Map map1){
		List list=new ArrayList<String>();
		for(Object key:map1.keySet()){  
			if("已扫描".equals(map1.get(key))){
				list.add(getKey(map, key.toString()));
			}
		} 
		String s=StringUtils.join(list.toArray(), ",");
		return s;
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, MaterialListActivity.class);  
        startActivity(intent);
        finish();
	}
	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		super.handleMessage(msg);
        		try {
        			String s= (String) msg.obj;
                    switch (msg.what) {
                    case 0:
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(MaterialIncomingActivity.this,"单据号有误!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    							objList = new JSONArray(ss.getString("list"));
    							String types="";
    							for (int i = 0; i< objList.length(); i++) {
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    				                JSONObject obj = objList.getJSONObject(i);
    				                types=obj.getString("status");
    				                try {
    				                	materialListData.put(obj.get("scanCode"), obj.getString("packageInventoryNum"));
    				                	materiaCheckData.put(obj.getString("packageInventoryNum"), types);
    								} catch (Exception e) {
    									// TODO: handle exception
    								}
    				             }
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    						if(myAdapter == null){
    							myAdapter = new AcceptanceAdapter(MaterialIncomingActivity.this, materiaCheckData);
    							listView.setAdapter(myAdapter);
    						}else{
    							myAdapter.setData(materiaCheckData) ;
    						}
    						allCounts.setText("已扫描个数："+counts);
    						myAdapter.notifyDataSetChanged();
    					}
        				break;
        			case 2:
        				if("2".equals(type)){
        					return;
        				}
        				//Toast.makeText(MaterialIncomingActivity.this,"xxx",Toast.LENGTH_SHORT).show();
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(MaterialIncomingActivity.this,"单据号有误!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    							objList = new JSONArray(ss.getString("list"));
    							String types="";
    							for (int i = 0; i< objList.length(); i++) {
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    				                JSONObject obj = objList.getJSONObject(i);
    				                types=obj.getString("state");
    				                if("0".equals(type)){
    				                	if("0".equals(types)){
    					                	types="可用 ";
    					                }else if("1".equals(types)){
    					                	types="正在使用";
    					                }else if("-1".equals(types)){
    					                	types="停用";
    					                }
    				                }else if("1".equals(type)){
    				                	types="待扫描";
    				                }
    				                
    				                try {
    				                	trayListData.put(obj.get("scanCode"), obj.getString("code"));
    				                	trayCheckData.put(obj.getString("code"), types);
    								} catch (Exception e) {
    									// TODO: handle exception
    								}
    				             }
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    						if("0".equals(type)){
    							if(checkAdapter == null){
    								checkAdapter = new CheckBoxAdapter(MaterialIncomingActivity.this, trayCheckData);
    								listView.setAdapter(checkAdapter);
    							}else{
    								checkAdapter.setData(trayCheckData) ;
    							}
    							checkAdapter.notifyDataSetChanged();
    						}else if("1".equals(type)){
    							if(myAdapter == null){
    								myAdapter = new AcceptanceAdapter(MaterialIncomingActivity.this, trayCheckData);
    								listView.setAdapter(myAdapter);
    							}else{
    								myAdapter.setData(trayCheckData) ;
    							}
    							allCounts.setText("已扫描个数："+counts);
    							myAdapter.notifyDataSetChanged();
    						}
    					}
        				break;
        			case 3:
        				//Toast.makeText(MaterialIncomingActivity.this,"xxx",Toast.LENGTH_SHORT).show();
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(MaterialIncomingActivity.this,"单据号有误!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    							objList = new JSONArray(ss.getString("list"));
    							String types="";
    							for (int i = 0; i< objList.length(); i++) {
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    				                JSONObject obj = objList.getJSONObject(i);
    				                types=obj.getString("positionState");
    				                if("0".equals(types)){
    				                	types="空";
    				                }else if("1".equals(types)){
    				                	types="非空";
    				                }else if("2".equals(types)){
    				                	types="冻结";
    				                }
    				                try {
    				                	storageListData.put(obj.get("scanCode"), obj.getString("positionCode"));
    				                	storageCheckData.put(obj.getString("positionCode"), types);
    								} catch (Exception e) {
    									// TODO: handle exception
    								}
    				             }
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    						if(checkAdapter == null){
    							checkAdapter = new CheckBoxAdapter(MaterialIncomingActivity.this, storageCheckData);
    							listView.setAdapter(checkAdapter);
    						}else{
    							checkAdapter.setData(storageCheckData) ;
    						}
    						allCounts.setText("已扫描个数："+counts);
    						checkAdapter.notifyDataSetChanged();
    					}
        				break;
        			default:
        				break;
        			}
				} catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(MaterialIncomingActivity.this,"单据号有误!",Toast.LENGTH_SHORT).show();
				}
            }

    };
}
