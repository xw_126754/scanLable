package com.UHF.scanlable;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.UHF.R;
import com.UHF.model.CheckBoxesAdapter;
import com.UHF.util.CheckDataFromServer;

public class ProductBoundDemandActivity extends Activity implements OnClickListener{

	Button refresh;
	Button confirmxx;
	/**
	 * 单据号/状态
	 */
	private LinkedHashMap<Object,Object> listData=new LinkedHashMap<Object,Object>();
	
	private LinkedHashMap allData=new LinkedHashMap();
	private CheckBoxesAdapter myAdapter;
	private ListView listView;
	private Intent intent;
	private String salesOrderId;
	private String orderId;
	private String orderState;
	private String outboundOrderId;
	//标题
	private TextView po_title;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_out_demand_list);
		salesOrderId=getIntent().getStringExtra("salesOrderId");
		orderId=getIntent().getStringExtra("orderId");
		orderState=getIntent().getStringExtra("orderState");
		outboundOrderId=getIntent().getStringExtra("outboundOrderId");
		refresh = (Button)findViewById(R.id.refresh2);
		refresh.setOnClickListener(this);
		confirmxx = (Button)findViewById(R.id.confirmxx);
		confirmxx.setOnClickListener(this);
		listView = (ListView)findViewById(R.id.materialList);//
		intent = new Intent(this,ProductOutActivity.class);
		po_title = (TextView)findViewById(R.id.po_title);//
		po_title.setText("成品出库--成品列表:"+orderId);
		/*listView.setOnItemClickListener(new OnItemClickListener(){  
            @Override  
            public void onItemClick(AdapterView<?> parent, View view,  
                    int position, long id) {  
        		String documentNo = myAdapter.getmList().get(position);
        		Map m=(Map) allData.get(documentNo);
        		intent.putExtra("goodsName", m.get("goodsName").toString());
        		intent.putExtra("goodsCode", m.get("goodsCode").toString());
        		intent.putExtra("demandCount", m.get("demandCount").toString());
//        		intent.putExtra("assignCount", m.get("assignCount").toString());
        		intent.putExtra("outboundDemandId", m.get("outboundDemandId").toString());
        		intent.putExtra("salesOrderId", salesOrderId);
        		intent.putExtra("orderId", orderId);
        		intent.putExtra("orderState", orderState);
        		intent.putExtra("outboundOrderId", outboundOrderId);
        		startActivity(intent);
                finish();
            }  
        });*/  
		refreshList();
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==refresh.getId()){
			myAdapter = null;
			listData.clear();
			allData.clear();
			refreshList();
		}
		if(v.getId()==confirmxx.getId()){
			List<String> documentNos= myAdapter.getChecked();
			if(documentNos.size()==0){
				Toast.makeText(ProductBoundDemandActivity.this,"请选择出库成品!",Toast.LENGTH_SHORT).show();
				return;
			}
			String outboundDemandIds="";
			String goodsCodes="";
			for(String doc:documentNos){
	    		Map m=(Map) allData.get(doc);
	    		if("".equals(outboundDemandIds)){
		    		goodsCodes=m.get("goodsCode").toString();
		    		outboundDemandIds=m.get("outboundDemandId").toString();
	    		}else{
		    		goodsCodes+=","+m.get("goodsCode").toString();
		    		outboundDemandIds+=","+m.get("outboundDemandId").toString();
	    		}
			}
//    		intent.putExtra("goodsName", m.get("goodsName").toString());
    		intent.putExtra("goodsCode",goodsCodes);
//    		intent.putExtra("demandCount", m.get("demandCount").toString());
//    		intent.putExtra("assignCount", m.get("assignCount").toString());
    		intent.putExtra("outboundDemandId", outboundDemandIds);
    		intent.putExtra("salesOrderId", salesOrderId);
    		intent.putExtra("orderId", orderId);
    		intent.putExtra("orderState", orderState);
    		intent.putExtra("outboundOrderId", outboundOrderId);
    		startActivity(intent);
            finish();
		}
	}
	public boolean refreshList(){
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
		m.put("salesOrderId", salesOrderId);//销售订单编号
		m.put("orderId", orderId);//订单编号
		m.put("orderState", orderState);//订单状态
		m.put("outboundOrderId", outboundOrderId);//订单状态
		m.put("user_id", UfhData.getEmployBean().getId());
		checkDataFromServer.setOperType("26");//成品入库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(0);
		checkDataFromServer.checkData();
		return true;
	}
	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, ProductOutListActivity.class);  
        startActivity(intent);
        finish();
	}


	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		super.handleMessage(msg);
                switch (msg.what) {
    			case 0:
    				try {
    					String s= (String) msg.obj;
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(ProductBoundDemandActivity.this,"无数据!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    		    				String operType=ss.getString("operType");
    							objList = new JSONArray(ss.getString("list"));
    							String types="";
    							for (int i = 0; i< objList.length(); i++) {
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    								Map<Object,Object> m=new HashMap<Object,Object>();
    				                JSONObject obj = objList.getJSONObject(i);
    				                types=obj.getString("demandState");
    				                if("0".equals(types)){
    				                	types="待出库";
    				                }else if("1".equals(types)){
    				                	types="进行中";
    				                }else if("2".equals(types)){
    				                	types="已完成";
    				                }else if("auditWarn".equals(types)){
    				                	types="已完成";
    				                }
    				                try {
    				    				m.put("goodsName", obj.getString("goodsName"));//货物名称
    				    				m.put("goodsCode", obj.getString("goodsCode"));//货物编号
    				    				m.put("demandCount", obj.getString("demandCount"));//需求量
//    				    				m.put("assignCount", obj.get("assignCount"));//实发量
    				    				m.put("outboundDemandId", obj.getString("outboundDemandId"));//物料单据id
    					                listData.put(obj.get("goodsCode"), types);
    					                allData.put(obj.get("goodsCode"), m);
    								} catch (Exception e) {
    									// TODO: handle exception
    								}
    				             }
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    						if(myAdapter == null){
    							myAdapter = new CheckBoxesAdapter(ProductBoundDemandActivity.this, listData);
    							listView.setAdapter(myAdapter);
    						}else{
    							myAdapter.setData(listData) ;
    						}
    						myAdapter.notifyDataSetChanged();
    			            
    					}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(ProductBoundDemandActivity.this,"无数据!",Toast.LENGTH_SHORT).show();
    				}
    				break;
    			case 1:
    				break;
    			case 2:
    				break;
    			default:
    				break;
    			}
            }

    };
}
